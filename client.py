import socket
import threading
from ProxySlot import ProxySlot


class Sender(ProxySlot):
    sock = socket.socket()

    def __init__(self):
        ProxySlot.__init__(self, self.sock, 4)

    def connect(self):
        self.sock.connect(('localhost', 9090))

    def disconnect(self):
        self.sock.close()

    def request(self, message):
        self.send(message)
        return self.recive()


sender = Sender()
sender.connect()
while True:
    message = input('>')
    if(message == "exit"):
        exit()
    sender.send(message)
    print(sender.recive())
