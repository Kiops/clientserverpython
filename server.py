import socket
import threading
from ProxySlot import ProxySlot

class ThreadedServer(object):

    data = {}

    def __init__(self, host, port):
        self.host = host
        self.port = port
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind((self.host, self.port))

    def listen(self):
        self.sock.listen(5)
        while True:
            client, address = self.sock.accept()
            # client.settimeout(600)
            client.setblocking(True)
            proxy_slot = ProxySlot(client, 4)
            threading.Thread(target = self.reciveMessage, args = (proxy_slot,)).start()

    def reciveMessage(self, client):
        try:
            while True:
                message = client.recive()
                if not message:
                    return
                self.processMessage(message, client)
        except:
            return

    def processMessage(self, message, client):
        words = message.split(" ")
        if(words[0] == "PUT" and len(words) >= 3):
            self.data[words[1]] = words[2]
            client.send("put")
        elif(words[0] == "GET" and len(words) >= 2):
            response = self.data.get(words[1])
            client.send("None" if response == None else response)
        elif(words[0] == "DELETE" and len(words) >= 2):
            del self.data[words[1]]
            client.send("deleted")

if __name__ == "__main__":
    ThreadedServer('',9090).listen()