class ProxySlot:
    helloSize = 4
    sock = None

    def __init__(self, sock, helloSize):
        self.sock = sock
        self.helloSize = helloSize

    def recive(self):
        try:
            helloMessage = self.sock.recv(self.helloSize)
            awaitingSize = int.from_bytes(helloMessage, byteorder='big', signed=False)
            return self.sock.recv(awaitingSize).decode()
        except Exception as e:
            raise e

    def send(self, pureMessage):
        try:
            message = pureMessage.encode()
            helloMessage = len(message).to_bytes(self.helloSize, byteorder='big')
            self.sock.send(helloMessage + message)
        except Exception as e:
            raise e